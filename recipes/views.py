from django.shortcuts import redirect
from django.db import IntegrityError
from django.urls import reverse_lazy
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, DeleteView, UpdateView

from recipes.forms import RatingForm
from django.contrib.auth.mixins import LoginRequiredMixin
from recipes.models import Recipe, ShoppingItem, Ingredient


class RecipeCreateView(LoginRequiredMixin, CreateView):
    model = Recipe
    template_name = "recipes/new.html"
    fields = ["name", "description", "servings", "tag", "image"]
    success_url = reverse_lazy("recipes_list")

    def form_valid(self, form):
        form.instance.author = self.request.user
        return super().form_valid(form)


class RecipeUpdateView(LoginRequiredMixin, UpdateView):
    model = Recipe
    template_name = "recipes/edit.html"
    fields = ["name", "author", "description", "servings", "tag", "image"]
    success_url = reverse_lazy("recipes_list")


class RecipeListView(ListView):
    model = Recipe
    template_name = "recipes/list.html"
    paginate_by = 2


class RecipeDetailView(DetailView):
    model = Recipe
    template_name = "recipes/detail.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["rating_form"] = RatingForm()
        foods = []
        if (self.request.user.is_authenticated):
            for item in self.request.user.shopping_items.all():
                foods.append(item.food_item)

        context["servings"] = self.request.GET.get("servings")

        context["food_in_shopping_list"] = foods
        return context


def log_rating(request, recipe_id):
    if request.method == "POST":
        form = RatingForm(request.POST)
        if form.is_valid():
            try:
                rating = form.save(commit=False)
                rating.recipe = Recipe.objects.get(pk=recipe_id)
                rating.save()
            except Recipe.DoesNotExist:
                return redirect('recipes_list')
    return redirect("recipe_detail", pk=recipe_id)


class RecipeDeleteView(LoginRequiredMixin, DeleteView):
    model = Recipe
    template_name = "recipes/delete.html"
    success_url = reverse_lazy("recipes_list")


def create_shopping_item(request):
    ingredient_id = request.POST.get("ingredient_id")
    # Get the value for the "ingredient_id" from the
    # request.POST dictionary using the "get" method

    # Get the specific ingredient from the Ingredient model
    # using the code
    ingredient = Ingredient.objects.get(id=ingredient_id)

    # Get the current user which is stored in request.user
    current_user = request.user

    try:
        # Create the new shopping item in the database
        ShoppingItem.objects.create(
          food_item=ingredient.food,
          user=current_user
        )
    except IntegrityError:  # Raised if someone tries to add
        pass                # the same food twice, just ignore it

    # Go back to the recipe page with a redirect
    # to the name of the registered recipe detail
    # path with code like this
    return redirect("recipe_detail", pk=ingredient.recipe.id)


class ShoppingItemListView(LoginRequiredMixin, ListView):
    model = ShoppingItem
    template_name = "shopping_items/list.html"

    def get_queryset(self):
        user = self.request.user
        return ShoppingItem.objects.filter(user=user)


def delete_all_shopping_items(request):
    # Delete all of the shopping items for the user
    current_user = request.user
    # using code like
    ShoppingItem.objects.filter(user=current_user).delete()

    # Go back to the shopping item list with a redirect
    # to the name of the registered shopping item list
    # path with code like this
    # return redirect(
    #     name of the registered shopping item list path
    # )
    return redirect("shopping_item_list")
