# Generated by Django 4.0.3 on 2022-04-29 23:36

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('meal_plans', '0002_meal_plan_image'),
    ]

    operations = [
        migrations.AddField(
            model_name='meal_plan',
            name='date',
            field=models.DateTimeField(null=True, verbose_name='%m/%d/%Y'),
        ),
    ]
