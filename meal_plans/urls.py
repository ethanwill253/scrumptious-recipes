from django.urls import path

from meal_plans.views import (
    MealCreateView,
    MealUpdateView,
    MealDeleteView,
    MealDetailView,
    MealListView,
)

urlpatterns = [
    path("", MealListView.as_view(), name="meal_list"),
    path("<int:pk>/", MealDetailView.as_view(), name="meal_detail"),
    path("create/", MealCreateView.as_view(), name="meal_new"),
    path("<int:pk>/edit/", MealUpdateView.as_view(), name="meal_edit"),
    path("<int:pk>/delete/", MealDeleteView.as_view(), name="meal_delete"),
]
