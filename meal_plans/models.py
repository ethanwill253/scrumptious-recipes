from django.conf import settings
from django.db import models


USER_MODEL = settings.AUTH_USER_MODEL


class Meal_Plan(models.Model):
    owner = models.ForeignKey(
        USER_MODEL,
        related_name="meals",
        on_delete=models.CASCADE,
        null=True,
    )
    name = models.CharField(max_length=120)
    description = models.TextField()
    image = models.URLField(null=True, blank=True)
    recipes = models.ManyToManyField("recipes.Recipe", related_name="meals")
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    date = models.DateField(
        blank=True,
        null=True,
        editable=True)

    def __str__(self):
        return self.name + " by " + str(self.owner)
