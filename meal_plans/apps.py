from django.apps import AppConfig


class Meal_PlanConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "meal_plans"
