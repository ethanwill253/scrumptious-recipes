from django.urls import reverse_lazy
from django.shortcuts import redirect
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, DeleteView, UpdateView

from django.contrib.auth.mixins import LoginRequiredMixin
from meal_plans.models import Meal_Plan


class MealCreateView(LoginRequiredMixin, CreateView):
    model = Meal_Plan
    template_name = "meal_plans/new.html"
    fields = ["name", "description", "image", "recipes", "date"]
    success_url = reverse_lazy("meal_list")

    def form_valid(self, form):
        plan = form.save(commit=False)
        plan.owner = self.request.user
        plan.save()
        form.save_m2m()
        return redirect("meal_detail", pk=plan.id)


class MealUpdateView(LoginRequiredMixin, UpdateView):
    model = Meal_Plan
    template_name = "meal_plans/edit.html"
    fields = ["name", "owner", "description", "image", "recipes", "date"]

    def get_success_url(self) -> str:
        return reverse_lazy("meal_detail", args=[self.object.id])


class MealListView(LoginRequiredMixin, ListView):
    model = Meal_Plan
    template_name = "meal_plans/list.html"
    paginate_by = 4

    def get_queryset(self):
        user = self.request.user
        return Meal_Plan.objects.filter(owner=user)


class MealDetailView(DetailView):
    model = Meal_Plan
    template_name = "meal_plans/detail.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return context


class MealDeleteView(LoginRequiredMixin, DeleteView):
    model = Meal_Plan
    template_name = "meal_plans/delete.html"
    success_url = reverse_lazy("meal_list")
