from django.contrib import admin

from meal_plans.models import Meal_Plan


# Register your models here.
class Meal_PlanAdmin(admin.ModelAdmin):
    pass


admin.site.register(Meal_Plan, Meal_PlanAdmin)
